package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Hangman {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        String word = keyboard.nextLine();
        List<Character> playerGuesses = new ArrayList<>();

        int wrongCount = 0;
        while(true) {
            printHangedMan(wrongCount);
            if(wrongCount>=6){
                System.out.println("Loseeeer!!!");
                break;
            }
            printWordState(word, playerGuesses);
            if(!getPlayerGuess(keyboard, word, playerGuesses)){
                wrongCount++;
            }

            if(printWordState(word, playerGuesses)){
                System.out.println("Winner!");
                break;
            }

            System.out.println("Guess the word:");
            if(keyboard.nextLine().equals(word)){
                System.out.println("Winner!");
                break;
            }else{
                System.out.println("Try again!");
            }
        }
    }

    public static boolean getPlayerGuess(Scanner keyboard, String word, List<Character> playerGuesses){
        System.out.println("Please enter a letter:");
        String letterGuess = keyboard.nextLine();
        playerGuesses.add(letterGuess.charAt(0));
        return word.contains(letterGuess);
    }

    public static boolean printWordState(String word, List<Character> playerGuesses){
        int correctCount = 0;
        for(int i=0; i<word.length(); i++){
            if(playerGuesses.contains(word.charAt(i))){
                System.out.print(word.charAt(i));
                correctCount++;
            }else{
                System.out.print("-");
            }
        }
        System.out.println();
        return word.length() == correctCount;
    }

    public static void printHangedMan(Integer wrongCount){
        System.out.println(" -------");
        System.out.println(" |     |");
        if(wrongCount>= 1){
            System.out.println(" O");
        }

        if(wrongCount>=2){
            System.out.print("\\ ");
            if(wrongCount >= 3){
                System.out.println("/");
            }else{
                System.out.println("");
            }
        }

        if(wrongCount >= 4){
            System.out.println(" |");
        }

        if(wrongCount>=5){
            System.out.print("/ ");
            if(wrongCount >= 6){
                System.out.println("\\");
            }else{
                System.out.println("");
            }
        }
        System.out.println();
        System.out.println();
    }

}
